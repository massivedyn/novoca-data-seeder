SET FOREIGN_KEY_CHECKS = 0;
SET @TRIGGER_DISABLE = 1;

-- ----------------------------
-- Hozzáférésre használt eszköz adatok
-- ----------------------------
CREATE TABLE `device` (
  `id`          varchar(64) NOT NULL,
  `platform`    varchar(255)         DEFAULT NULL
  COMMENT 'Platform adatok a következő alakban: "[OS]:[verzió]:[kategória]"; Pl: "Android:4.4.2:tablet"',
  `browser`     varchar(255)         DEFAULT NULL
  COMMENT 'Megjelenítő szoftver a következő alakban: "[Név]:[verzió]"; Pl: "Chrome:50.43.24"',
  `time_update` datetime             DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create` datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  COMMENT ='Eszköz adatok';

-- ----------------------------
-- Felhasználók
-- ----------------------------
CREATE TABLE `user` (
  `id`          int(10) unsigned             NOT NULL AUTO_INCREMENT,
  `type`        enum ('guest', 'registered') NOT NULL DEFAULT 'guest'
  COMMENT 'Vendég vagy regisztrált felhasználó',
  `email`       varchar(255)                          DEFAULT NULL
  COMMENT 'Kapcsolattartási email',
  `state`       enum ('inactive', 'active')  NOT NULL DEFAULT 'inactive',
  `time_update` datetime                              DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create` datetime                     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  COMMENT ='Felhasználók';

-- ----------------------------
-- Felhasználó authentikációs adatok
-- ----------------------------
CREATE TABLE `user_identity` (
  `user_id`     int(10) unsigned                                 NOT NULL,
  `type`        enum ('account', 'device', 'facebook', 'google') NOT NULL DEFAULT 'account'
  COMMENT 'account: azonosító/jelszó belépés; device: eszköz azonosító lapú (csak vendégeknek)',
  `name`        varchar(128)                                     NOT NULL
  COMMENT 'Identitás azonosító',
  `password`    varchar(255)                                              DEFAULT NULL
  COMMENT 'Identitást validáló kulcs ha kell',
  `time_update` datetime                                                  DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create` datetime                                         NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`, `type`),
  UNIQUE KEY `idx_type_name` (`type`, `name`) USING BTREE,
  CONSTRAINT `user_identity_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  COMMENT ='Felhasználót authentikáló kulcsok, azonosítók';

-- ----------------------------
-- Hozzáférésre használt munkamenet azonositók
-- ----------------------------
CREATE TABLE `token` (
  `id`          varchar(128)                NOT NULL,
  `password`    varchar(255)                         DEFAULT NULL
  COMMENT 'Lejárati idő frissítéséhez (bővítéséhez) használható "jelszó" (tárolás: hash+salt)',
  `user_id`     int(10) unsigned                     DEFAULT NULL,
  `device_id`   varchar(64)                 NOT NULL,
  `ip`          varchar(39)                          DEFAULT NULL,
  `data`        text,
  `state`       enum ('inactive', 'active') NOT NULL DEFAULT 'inactive',
  `refresh`     tinyint(3) unsigned         NOT NULL DEFAULT '0'
  COMMENT 'Lejárati idő frissítéseinek száma',
  `time_expire` datetime                             DEFAULT NULL
  COMMENT 'Lejárat időpontja',
  `time_update` datetime                             DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create` datetime                    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_state_timeexpire` (`state`, `time_expire`) USING BTREE,
  KEY `idx_deviceid` (`device_id`) USING BTREE,
  KEY `idx_userid` (`user_id`),
  CONSTRAINT `token_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `token_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
  ENGINE = InnoDB
  COMMENT ='API hozzáférésre használt azonosítók';

-- ----------------------------
-- Naplóbejegyzések
-- ----------------------------
CREATE TABLE `log` (
  `name`          varchar(255) NOT NULL DEFAULT '',
  `type`          varchar(60)  NOT NULL,
  `message_raw`   text,
  `message`       text,
  `data`          longtext,
  `time_accurate` double unsigned       DEFAULT NULL,
  `time_create`   datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Témakörök. A `_word` érték a `word_category` tábla módositásakor változik
-- ----------------------------
CREATE TABLE `category` (
  `id`          smallint(5) unsigned        NOT NULL AUTO_INCREMENT,
  `name`        varchar(255)                NOT NULL
  COMMENT 'Adminisztratív név',
  `title`       varchar(255)                NOT NULL
  COMMENT 'Megjelenített név',
  `state`       enum ('active', 'inactive') NOT NULL DEFAULT 'inactive',
  `_word`       mediumint(8) unsigned       NOT NULL DEFAULT 0,
  `time_update` datetime                             DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create` datetime                    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  COMMENT ='Témakörök';

-- ----------------------------
-- Angol szavak. Egy szó csak egyszer szerepelhet, de szükséges az id, hogy kevesebb problámával tudjuk őket további táblákhoz kapcsolni
-- ----------------------------
CREATE TABLE `word` (
  `id`          mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title`       varchar(255)          NOT NULL,
  `_hash`       varchar(40)           NULL,
  `time_update` datetime              NULL     DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create` datetime              NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idx_hash` (`_hash`) USING BTREE
)
  COMMENT ='Angol szavak';

DROP TRIGGER IF EXISTS `word-insert-_hash`;
CREATE TRIGGER `word-insert-_hash`
  BEFORE INSERT
  ON `word`
  FOR EACH ROW
  SET NEW._hash = SHA1(LOWER(TRIM(NEW.title)));

DROP TRIGGER IF EXISTS `word-update-_hash`;
CREATE TRIGGER `word-update-_hash`
  BEFORE UPDATE
  ON `word`
  FOR EACH ROW
  SET NEW._hash = SHA1(LOWER(TRIM(NEW.title)));

-- ----------------------------
-- Angol szavak magyar jelentései
-- ----------------------------
CREATE TABLE `word_translation` (
  `id`          int(10) UNSIGNED      NOT NULL AUTO_INCREMENT,
  `word_id`     mediumint(8) UNSIGNED NOT NULL,
  `title`       VARCHAR(255)          NOT NULL,
  `time_update` datetime                       DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create` datetime              NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`word_id`) REFERENCES `word` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  INDEX `idx_wordid` (`word_id`) USING BTREE
)
  COMMENT ='Szavakhoz rendelt jelentések';

-- ----------------------------
-- Szavak témakörökhöz rendelései
-- ----------------------------
CREATE TABLE `word_category` (
  `category_id` smallint(5) UNSIGNED  NOT NULL,
  `word_id`     mediumint(8) UNSIGNED NOT NULL,
  `time_create` datetime              NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`, `word_id`),
  FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (`word_id`) REFERENCES `word` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  INDEX `idx_wordid` (`word_id`) USING BTREE
)
  COMMENT ='Szavakhoz rendelt témakörök';

DROP TRIGGER IF EXISTS `word_category-insert-category`;
CREATE TRIGGER `word_category-insert-category`
  AFTER INSERT
  ON `word_category`
  FOR EACH ROW UPDATE category AS c
SET _word = (SELECT COUNT(0)
             FROM word_category
             WHERE category_id = c.id)
WHERE id IN (NEW.category_id);

DROP TRIGGER IF EXISTS `word_category-update-category`;
CREATE TRIGGER `word_category-update-category`
  AFTER UPDATE
  ON `word_category`
  FOR EACH ROW UPDATE category AS c
SET _word = (SELECT COUNT(0)
             FROM word_category
             WHERE category_id = c.id)
WHERE id IN (NEW.category_id, OLD.category_id);

DROP TRIGGER IF EXISTS `word_category-remove-category`;
CREATE TRIGGER `word_category-remove-category`
  AFTER UPDATE
  ON `word_category`
  FOR EACH ROW UPDATE category AS c
SET _word = (SELECT COUNT(0)
             FROM word_category
             WHERE category_id = c.id)
WHERE id IN (OLD.category_id);

-- ----------------------------
-- Felhasználó által a szótárba helyezett magyar jelentések. Azért a jelentések kerülnek mentésre itt,
-- mert azokból meghatározhatóak a szótárba helyezett szavak is
-- ----------------------------
CREATE TABLE `user_word_translation` (
  `translation_id` INT(10) UNSIGNED NOT NULL,
  `user_id`        INT(10) UNSIGNED NOT NULL,
  `_failure`       SMALLINT(5)      NOT NULL     DEFAULT 0
  COMMENT 'Sikertelen megoldások száma (resetel, ha sikerül egy feladatban)',
  `time_update`    DATETIME         NULL         DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create`    DATETIME         NOT NULL     DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`translation_id`, `user_id`),
  FOREIGN KEY (`translation_id`) REFERENCES `word_translation` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_userid` (`user_id`) USING BTREE
)
  COMMENT ='Felhasználó szótára';

-- ----------------------------
-- Felhasználók által létrehozott egyedi listák
-- ----------------------------
CREATE TABLE `user_list` (
  `id`          INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id`     INT(10) UNSIGNED NOT NULL,
  `title`       VARCHAR(255)     NOT NULL,
  `time_update` DATETIME         NULL     DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create` DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_userid` (`user_id`) USING BTREE
)
  COMMENT ='Felhasználó egyedi listák';

-- ----------------------------
-- Felhasználó egyedi listáira kötött szavak (minden jelentést tartalmaz, nincs külön jelentés kiválasztás)
-- ----------------------------
CREATE TABLE `user_list_word` (
  `list_id`     int(10) UNSIGNED      NOT NULL,
  `word_id`     mediumint(8) UNSIGNED NOT NULL,
  `time_create` datetime              NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`word_id`, `list_id`),
  FOREIGN KEY (`word_id`) REFERENCES `word` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (`list_id`) REFERENCES `user_list` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_listid` (`list_id`) USING BTREE
)
  COMMENT ='Felhasználó listákhoz tartozó szavak';

-- ----------------------------
-- Felhasználó által kitöltött feladatok és ezek adatai
-- ----------------------------
CREATE TABLE `user_exercise` (
  `id`          mediumint(8) UNSIGNED   NOT NULL AUTO_INCREMENT,
  `user_id`     int(10) UNSIGNED        NOT NULL,
  `type`        enum ('repeat', 'test') NOT NULL,
  `timer`       MEDIUMINT(5)            NULL     DEFAULT NULL,
  `meta`        JSON                    NULL     DEFAULT NULL
  COMMENT '{ list: []"felhasznált egyéni listák", category_list: []"felhasznált témakörök" }',
  `time_begin`  datetime                NULL     DEFAULT NULL,
  `time_end`    datetime                NULL     DEFAULT NULL,
  `time_update` datetime                NULL     DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create` datetime                NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_userid` (`user_id`) USING BTREE
)
  COMMENT ='Felhasználó által összeállitott feladatok adatai';

-- ----------------------------
-- Feladatokban feltett kérdések adatai. Az `_answer` és `_correct` adatok a `user_exercise_question_answer` táblából kerülnek kiszámitásra
-- ----------------------------
CREATE TABLE `user_exercise_question` (
  `id`          int(10) UNSIGNED                                         NOT NULL AUTO_INCREMENT,
  `exercise_id` mediumint(8) UNSIGNED                                    NOT NULL,
  `type`        enum ('shuffle', 'blank', 'crossword', 'en-hu', 'hu-en') NOT NULL,
  `_answer`     SMALLINT(3)                                              NOT NULL
  COMMENT 'Lehetséges válaszok száma',
  `_correct`    SMALLINT(3)                                              NOT NULL DEFAULT 0
  COMMENT 'Helyes válaszok száma',
  `time_begin`  datetime                                                 NULL     DEFAULT NULL,
  `time_end`    datetime                                                 NULL     DEFAULT NULL,
  `time_update` datetime                                                 NULL     DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create` datetime                                                 NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`exercise_id`) REFERENCES `user_exercise` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `idx_exerciseid` (`exercise_id`) USING BTREE
)
  COMMENT ='Feladatokban szereplő kérdések adatai';

-- ----------------------------
-- Feladatokban feltett kérdésekre adott válaszok (felhasználó által)
-- ----------------------------
CREATE TABLE `user_exercise_question_answer` (
  `question_id`     int(10) UNSIGNED       NOT NULL,
  `word_id`         mediumint(8) UNSIGNED  NOT NULL AUTO_INCREMENT,
  `input`           VARCHAR(255)           NULL     DEFAULT NULL
  COMMENT 'Felhasználó által megadott megfejtés betűsorrend',
  `input_translate` VARCHAR(255)           NULL     DEFAULT NULL
  COMMENT 'Felhasználó megadott jelentés ha van ilyen',
  `meta`            JSON                   NULL     DEFAULT NULL
  COMMENT '{ translation_list: []"Szó elérhető jelentései a szótárban", word: ''''"Felhasználónak megjelenitett szó (hiányos betűkkel vagy keverve)" }',
  `state`           enum ('error', 'done') NOT NULL,
  `ordering`        int(10)                NULL     DEFAULT NULL,
  `time_update`     datetime               NULL     DEFAULT NULL
  ON UPDATE CURRENT_TIMESTAMP,
  `time_create`     datetime               NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`word_id`, `question_id`),
  FOREIGN KEY (`question_id`) REFERENCES `user_exercise_question` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (`word_id`) REFERENCES `word` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  INDEX `idx_questionid` (`question_id`) USING BTREE
)
  COMMENT ='Feladatok kérdéseire várt válaszok és a felhasználó inputjai';

DROP TRIGGER IF EXISTS `user_exercise_question_answer-insert-user_exercise_question`;
CREATE TRIGGER `user_exercise_question_answer-insert-user_exercise_question`
  AFTER INSERT
  ON `user_exercise_question_answer`
  FOR EACH ROW UPDATE user_exercise_question AS ueq
SET `_answer` = (SELECT COUNT(0)
                 FROM user_exercise_question_answer
                 WHERE question_id = ueq.id), `_correct` = (SELECT COUNT(0)
                                                            FROM user_exercise_question_answer
                                                            WHERE question_id = ueq.id AND state = 'done')
WHERE id IN (NEW.question_id);

DROP TRIGGER IF EXISTS `user_exercise_question_answer-update-user_exercise_question`;
CREATE TRIGGER `user_exercise_question_answer-update-user_exercise_question`
  AFTER UPDATE
  ON `user_exercise_question_answer`
  FOR EACH ROW UPDATE user_exercise_question AS ueq
SET `_answer` = (SELECT COUNT(0)
                 FROM user_exercise_question_answer
                 WHERE question_id = ueq.id), `_correct` = (SELECT COUNT(0)
                                                            FROM user_exercise_question_answer
                                                            WHERE question_id = ueq.id AND state = 'done')
WHERE id IN (NEW.question_id, OLD.question_id);

DROP TRIGGER IF EXISTS `user_exercise_question_answer-remove-user_exercise_question`;
CREATE TRIGGER `user_exercise_question_answer-remove-user_exercise_question`
  AFTER UPDATE
  ON `user_exercise_question_answer`
  FOR EACH ROW UPDATE user_exercise_question AS ueq
SET `_answer` = (SELECT COUNT(0)
                 FROM user_exercise_question_answer
                 WHERE question_id = ueq.id), `_correct` = (SELECT COUNT(0)
                                                            FROM user_exercise_question_answer
                                                            WHERE question_id = ueq.id AND state = 'done')
WHERE id IN (OLD.question_id);

-- ----------------------------
-- Define the new version
-- ----------------------------
DROP FUNCTION IF EXISTS _version;
DELIMITER $$
CREATE FUNCTION _version()
  RETURNS VARCHAR(32) NO SQL DETERMINISTIC
  BEGIN
    RETURN '1.0.0';
  END
$$
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
SET @TRIGGER_DISABLE = 0;
