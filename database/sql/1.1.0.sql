SET FOREIGN_KEY_CHECKS = 0;
SET @TRIGGER_DISABLE = 1;

-- ----------------------------
-- A szavakat és jelentéseiket is ki kell egészíteni pre és postfix lehetőségekkel, illetve egyéni hozzáfűzéssel
-- ----------------------------
ALTER TABLE word ADD comment text DEFAULT NULL NULL AFTER title;
ALTER TABLE word ADD postfix varchar(255) DEFAULT NULL  NULL AFTER title;
ALTER TABLE word ADD prefix varchar(255) DEFAULT NULL NULL AFTER title;

ALTER TABLE word_translation ADD comment text DEFAULT NULL NULL AFTER title;
ALTER TABLE word_translation ADD postfix varchar(255) DEFAULT NULL  NULL AFTER title;
ALTER TABLE word_translation ADD prefix varchar(255) DEFAULT NULL NULL AFTER title;

-- ----------------------------
-- A szavakhoz típusok (többnyire szófaj) hozzáadása, és így együtt egyediek ezentúl
-- ----------------------------
ALTER TABLE word ADD type varchar(40) NOT NULL AFTER id;
DROP INDEX idx_hash ON word;
CREATE UNIQUE INDEX idx_type_hash ON word (type, `_hash`);

-- ----------------------------
-- Egyéni listák átnevezése, mert a 'list' túl általános volt nem lehetett kódokban jól használni
-- ----------------------------
ALTER TABLE user_list RENAME user_dictionary;
ALTER TABLE user_list_word RENAME user_dictionary_word;

ALTER TABLE user_dictionary_word CHANGE list_id dictionary_id int(10) unsigned NOT NULL;
CREATE INDEX idx_dictionaryid ON user_dictionary_word (dictionary_id);
DROP INDEX idx_listid ON user_dictionary_word;

-- ----------------------------
-- Define the new version
-- ----------------------------
DROP FUNCTION IF EXISTS _version;
DELIMITER $$
CREATE FUNCTION _version()
  RETURNS VARCHAR(32) NO SQL DETERMINISTIC
  BEGIN
    RETURN '1.1.0';
  END
$$
DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;
SET @TRIGGER_DISABLE = 0;
