# Novoca Data Seeder

A Larave framework based excel to mysql parser.

### Prerequisites

```
Composer
PHP 7.2
```

### Installing

```
Copy the .env.example, rename to .env

You must fill out the database related environment variables

Init your database with your init scripts

Composer install
```

Note: In most cases the DB_DUMP_HOST, DB_DUMP_PORT, are the same as the DB_HOST and the DB_PORT.
The DB_MYSQLDUMP_LOCATION is where your mysqldump binary located.

Also, you should adjust your PHP's max_execution_time and memory_limit in your .ini (the parsing could take several minues)

## Running

You should open the project's public folder in your web browser.