<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;
use File;
use Session;
use App\Word;
use App\Category;
use App\WordTranslation;
use App\WordCategory;
use Exception;
use Log;
use Carbon\Carbon;

define('CONTINUE_IMPORT_FROM_LINE_X', false);
// you must not count the empty lines (rows)
define('CONTINUE_IMPORT_FROM_LINE_NUMBER', 12);

class ExcelMigrationController extends Controller
{

    public function index()
    {
        return view('import');
    }

    public function import(Request $request)
    {
        $extension = File::extension($request->file->getClientOriginalName());

        if (!$extension == "xlsx")
        {
            Session::flash('error', 'Please upload a valid xlsx file.');
            return back();
        }

        $startOfTheImport = Carbon::now();

        $path = $request->file->getRealPath();

        $data = (new FastExcel)->withoutHeaders()->sheet(1)->import($path);

        if (CONTINUE_IMPORT_FROM_LINE_X)
        {
            $parsedRows = $data->slice(CONTINUE_IMPORT_FROM_LINE_NUMBER);
        } else
        {
            $parsedRows = $data->slice(2);
            $this->truncateOldData();
        }

        foreach ($parsedRows as $parsedRow)
        {
            $parsedRow[0] = trim($parsedRow[0]);
            $parsedRow[1] = trim($parsedRow[1]);
            $parsedRow[2] = trim($parsedRow[2]);
            $parsedRow[3] = trim($parsedRow[3]);
            $parsedRow[4] = trim($parsedRow[4]);
            if (is_string($parsedRow[5]))
            {
                $parsedRow[5] = trim($parsedRow[5]);
            }
            if (is_string($parsedRow[6]))
            {
                $parsedRow[6] = trim($parsedRow[6]);
            }
            if (isset($parsedRow[7]) && is_string($parsedRow[7]))
            {
                $parsedRow[7] = trim($parsedRow[7]);
            }
            if (isset($parsedRow[8]))
            {
                $parsedRow[8] = trim($parsedRow[8]);
            }


            try
            {
                if (!empty($parsedRow[0]))
                {
                    $currentCategory = Category::where('title', $parsedRow[0])->first();

                    if (!$currentCategory)
                    {
                        $currentCategory = Category::create(['title' => $parsedRow[0], 'name' => $parsedRow[0], 'state' => 'active', '_word' => 0]);
                    }

                    if ($parsedRow[2])
                    {
                        $found = false;

                        $words = Word::where('title', $parsedRow[2])->where('type', ($parsedRow[4]) ? (substr($parsedRow[4], 1, -1)) : '-')->get();

                        foreach ($words as $word)
                        {
                            $trans1Match = false;
                            $trans2Match = false;
                            $trans3Match = false;

                            $word = $word->load('translations');

                            if ($parsedRow[5] && $word->translations->get(0))
                            {
                                if ($word->translations->get(0)->title == $parsedRow[5])
                                {
                                    $trans1Match = true;
                                }
                            } else
                            {
                                if (!$parsedRow[5] && !$word->translations->get(0))
                                {
                                    $trans1Match = true;
                                }
                            }

                            if ($parsedRow[6] && $word->translations->get(1))
                            {
                                if ($word->translations->get(1)->title == $parsedRow[6])
                                {
                                    $trans2Match = true;
                                }
                            } else
                            {
                                if (!$parsedRow[6] && !$word->translations->get(1))
                                {
                                    $trans2Match = true;
                                }
                            }

                            if ($parsedRow[7] && $word->translations->get(2))
                            {
                                if ($word->translations->get(2)->title == $parsedRow[7])
                                {
                                    $trans3Match = true;
                                }
                            } else
                            {
                                if (!$parsedRow[7] && !$word->translations->get(2))
                                {
                                    $trans3Match = true;
                                }
                            }

                            if (!(!$trans1Match || !$trans2Match || !$trans3Match))
                            {
                                $found = true;
                                break;
                            }
                        }
                        if (!$found)
                        {
                            $word = Word::create(['prefix' => $parsedRow[1], 'title' => $parsedRow[2], 'postfix' => $parsedRow[3], 'type' => ($parsedRow[4]) ? (substr($parsedRow[4], 1, -1)) : '-']);
                            $currentCategory->words()->attach($word->id);
                        }

                        if ($found)
                        {
                            $categoryFound = false;

                            foreach ($word->categories as $category)
                            {
                                if ($category->title == $parsedRow[0])
                                {
                                    $categoryFound = true;
                                }
                            }
                            if (!$categoryFound)
                            {
                                $currentCategory->words()->attach($word->id);
                            }
                        }

                        if ($parsedRow[5])
                        {
                            $this->createAndAttachWordTranslation($word, $parsedRow, $parsedRow[5]);
                        }
                        if ($parsedRow[6])
                        {
                            $this->createAndAttachWordTranslation($word, $parsedRow, $parsedRow[6]);
                        }
                        if ($parsedRow[7])
                        {
                            $this->createAndAttachWordTranslation($word, $parsedRow, $parsedRow[7]);
                        }
                    }
                }
            }
            catch (Exception $e)
            {
                Log::useDailyFiles(storage_path().'/logs/debug.log');
                Log::info(['Error parsing the following row' => $parsedRow]);
            }
        }

        if (CONTINUE_IMPORT_FROM_LINE_X)
        {
            $this->deleteOldData($startOfTheImport);
        }

        Session::flash('success', 'Your Data has successfully imported');

        $filename = "backup-" . date("d-m-Y") . ".sql";
        $mime = "application/x-gzip";

        header( "Content-Type: " . $mime );
        header( 'Content-Disposition: attachment; filename="' . $filename . '"' );

        $cmd = env('DB_MYSQLDUMP_LOCATION') . " --no-create-info --complete-insert --skip-triggers -P " . env('DB_DUMP_PORT') . " -h " . env('DB_DUMP_HOST') . " -u " . env('DB_USERNAME') . " --password=" . env('DB_PASSWORD') . " " . env('DB_DATABASE');

        passthru( $cmd );

        exit(0);
    }

    private function createAndAttachWordTranslation($word, $parsedRow, $translationTitle)
    {
        $sameTranslationFound = false;

        foreach ($word->translations as $translation)
        {
            if ($translationTitle == $translation->title)
            {
                $sameTranslationFound = true;
            }
        }

        if (!$sameTranslationFound)
        {
            $translation = WordTranslation::create(['word_id' => $word->id, 'title' => $translationTitle, 'postfix' => (isset($parsedRow[8])) ? $parsedRow[8] : null]);
        }

        $sameTranslationFound = false;
    }

    private function truncateOldData()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('word_category')->truncate();
        DB::table('word_translation')->truncate();
        DB::table('word')->truncate();
        DB::table('category')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    private function deleteOldData($startOfTheImport)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        WordCategory::where('time_create', '<=', $startOfTheImport)->delete();
        WordTranslation::where('time_create', '<=', $startOfTheImport)->delete();
        Word::where('time_create', '<=', $startOfTheImport)->delete();
        Category::where('time_create', '<=', $startOfTheImport)->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
