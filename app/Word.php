<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $table = 'word';

    protected $fillable = [
        'prefix',
        'title',
        'postfix',
        'type',
    ];

    public $timestamps = false;

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'word_category');
    }

    public function translations()
    {
        return $this->hasMany('App\WordTranslation');
    }
}
