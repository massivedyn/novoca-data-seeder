<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WordTranslation extends Model
{
    protected $table = 'word_translation';

    protected $fillable = [
        'title',
        'postfix',
        'word_id'
    ];

    public $timestamps = false;

    public function word()
    {
        return $this->belongsTo('App\Word');
    }
}
