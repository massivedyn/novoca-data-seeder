<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WordCategory extends Model
{
    protected $table = 'word_category';

    public $timestamps = false;
}
