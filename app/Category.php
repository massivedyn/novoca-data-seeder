<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $fillable = [
        'title',
        'name',
        'state',
        '_word',
    ];

    public $timestamps = false;

    public function words()
    {
        return $this->belongsToMany('App\Word', 'word_category');
    }
}
