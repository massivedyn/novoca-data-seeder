#!/usr/bin/env bash

#
function do_writeable() {

  local group="$1"
  shift

  for path in ${@}
  do
    if [[ "${path}" =~ \/$ ]]; then mkdir -p "$@"; else touch "$@"; fi
  done

  [ ! -z "${group}" ] && chown :${group} -f -R "$@"
}

### EDIT after this section to add pre-run scripts

# [service-database] Setup directories and permissions for MySQL service
do_writeable "${DOKERIZER_GROUP_999}" database/mysql/{data,log}/

# [service-web]
do_writeable "${DOKERIZER_GROUP_33}" web/{php,apache}/log/

###

# [service-web]
do_writeable "${DOKERIZER_GROUP_33}" ../storage/

exit 0
