#!/usr/bin/env bash

echo "Running in '${DOCKERAPP_ENVIRONMENT:-development}' environment.."

# Wait for other services to startup
wait-for-it database:3306 -t 0 || exit 1

# Run database update/import
dokerizer-database update "${DOCKERAPP_GROUP}" "/var/www/html/database/sql/" "${DOCKERAPP_VERSION}" || exit 1
dokerizer-database import "/var/www/html/database/sql/import/" || exit 1

# Composer update/install
composer install --no-interaction --prefer-dist ${DOCKERAPP_ENVIRONMENT:+--no-dev}  || exit 1

# create .env for Laravel
if [ ! -f "/var/www/html/.env" ]; then
  echo "APP_NAME=${DOCKERAPP_TITLE}
APP_ENV=${DOCKERAPP_ENVIRONMENT:-development}
APP_KEY=
APP_URL=http://${DOCKERAPP_DOMAIN}
APP_DEBUG=true
APP_LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=database
DB_DUMP_HOST=database
DB_PORT=3306
DB_DUMP_PORT=3306
DB_DATABASE=${DOCKERAPP_GROUP}
DB_USERNAME=${DOKERIZER_DATABASE_USER}
DB_PASSWORD=${DOKERIZER_DATABASE_PASSWORD}
DB_MYSQLDUMP_LOCATION=/usr/bin/mysqldump

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync" >> "/var/www/html/.env"

php artisan key:generate
fi

# Remove empty media directories (TODO maybe this should be in a cron)
#(find /var/www/html/media /var/www/html/tmp/media/ -type d -empty -delete 2>/dev/null)

# Start application workers (initial watchdog call)
#([ -d /var/www/html/tmp/worker/ ] && rm -rf /var/www/html/tmp/worker/)
#dockerapp-watchdog

#
#cron

# Start apache in the foreground
echo "Starting Apache, listening for connections.."
umask 002
apache2-foreground